<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Tech-Hospital</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <?php wp_head(); ?>
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>



<body>
	<div class="fluid-container">
		<header class="header">
			<div class="row j-center">
        <a href="<?php echo esc_url(home_url('/'));?>">
  				<figure class="logo">
  					<img class="logo__img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt="">
  				</figure>

        </a>
			</div>
			<div class="clase">
	 
		 </div>
      <div class="row">
      <?php if ( is_user_logged_in() ) {?>
  			<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Ingresar dispositivo' ) ) ); ?>" class="t-black col-7"><i class="fa fa-plus b-red t-white t-hover-white b-hover-black add-device"></i>Dispositivo</a><?php } else { ?><div class="col-7"></div> <?php } ?>
        
      </div>


			<nav class="navbar navbar-expand-lg b-black">
        <button class="navbar-toggler nav__icon" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <i class="fa fa-bars t-white"></i>
        </button>
        <div id="collapsibleNavbar" class="collapse navbar-collapse header__nav row b-black t-white j-center">
          <ul id="main__menu" class="nav navbar-nav">
            <li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-18">
              <a href="https://www.tech-hospital.com/" class="nav-link t-white b-hover-red t-hover-white">Inicio</a>
            </li>
            <li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17">
              <a href="https://www.tech-hospital.com/sobre-nosotros/" class="nav-link t-white b-hover-red t-hover-white">Sobre Nosotros</a>
            </li>
            <li id="menu-item-44" class=" dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-44">
              <a href="#" class="nav-link t-white b-hover-red t-hover-white">Productos</a>
              <div class="dropdown-content">
                <a href="<?php echo esc_url(home_url('/apple'));?>">Apple</a>
                <a href="<?php echo esc_url(home_url('/hp'));?>">Hp</a>
                <a href="<?php echo esc_url(home_url('/acer'));?>">Acer</a>
                <a href="<?php echo esc_url(home_url('/benq'));?>">Proyectores</a>
              </div>
            </li>
            <li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16">
              <a href="https://www.tech-hospital.com/cotizar/" class="nav-link t-white b-hover-red t-hover-white">Cotizar</a>
            </li>
            <li id="menu-item-135" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135">
              <a href="https://www.tech-hospital.com/blog/" class="nav-link t-white b-hover-red t-hover-white">Blog</a>
            </li>
			  <li id="menu-item-135" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135">
              <a href="https://www.tech-hospital.com/password/" class="nav-link t-white b-hover-red t-hover-white">Tienda</a>
            </li>
          </ul>
        </div>
      </nav>

        <!-- <nav class="navbar navbar-expand-lg b-black">
          <button class="navbar-toggler nav__icon" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <i class="fa fa-bars t-white"></i>
          </button>
          <?php wp_nav_menu( array(
          'theme_location' => 'header-menu',
          'container' => 'div',
          'container_class' => 'collapse navbar-collapse header__nav row b-black t-white j-center',
          'container_id' => 'collapsibleNavbar',
          'items_wrap' => '<ul id="main__menu" class="nav navbar-nav">%3$s</ul>',
          'menu_class' => 'nav__item nav-item'
          )); ?>
        </nav> -->
		</header>
		<section class="main__content">














	

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		
